package de.fjobilabs.usernamedatabasetools.database;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 11:51:22
 */
public class DatabaseException extends Exception {
    
    private static final long serialVersionUID = -8792390429410012336L;
    
    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
