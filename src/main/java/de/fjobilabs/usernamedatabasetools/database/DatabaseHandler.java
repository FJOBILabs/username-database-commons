package de.fjobilabs.usernamedatabasetools.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 16.12.2017 - 11:45:46
 */
public class DatabaseHandler {
    
    private DatabaseConfiguration configuration;
    private String connectionUrl;
    private Connection connection;
    private String batchSQLStatement;
    private PreparedStatement batch;
    
    public DatabaseHandler(DatabaseConfiguration configuration) throws DatabaseException {
        this.configuration = configuration;
        this.connectionUrl = buildConnectionUrl();
        this.connection = createConnection();
        this.batchSQLStatement = buildBatchSQLStatement();
    }
    
    public int getMaxSectionId() throws DatabaseException {
        try {
            Statement statement = this.connection.createStatement();
            ResultSet resultSet = statement.executeQuery(buildSelectMaxSectionSQLStatement());
            int section = 0;
            if (resultSet.next()) {
                section = resultSet.getInt(1);
            }
            return section;
        } catch (SQLException e) {
            throw new DatabaseException("Failed to get next section ID!", e);
        }
    }
    
    public void startBatch() throws DatabaseException {
        if (this.batch != null) {
            throw new IllegalStateException("Batch already started");
        }
        try {
            this.batch = this.connection.prepareStatement(this.batchSQLStatement);
        } catch (SQLException e) {
            throw new DatabaseException("Failed to prepare batch statement", e);
        }
    }
    
    public void addUsername(String username, int section) throws DatabaseException {
        try {
            this.batch.setString(1, username);
            this.batch.setInt(2, section);
            this.batch.addBatch();
        } catch (SQLException e) {
            throw new DatabaseException("Failed to add username to batch", e);
        }
    }
    
    public void executeBatch() throws DatabaseException {
        if (this.batch == null) {
            throw new IllegalStateException("No batch started");
        }
        try {
            this.batch.executeBatch();
        } catch (SQLException e) {
            throw new DatabaseException("Failed to executeBatch", e);
        }
        try {
            this.batch.close();
        } catch (SQLException e) {
            System.err.println("Failed to close batch");
            e.printStackTrace();
        }
        this.batch = null;
    }
    
    public Collection<String> getUsernames(int section) throws DatabaseException {
        Statement statement;
        try {
            statement = this.connection.createStatement();
        } catch (SQLException e) {
            throw new DatabaseException("Failed to create statement", e);
        }
        Set<String> usernames = new HashSet<>();
        try {
            ResultSet result = statement.executeQuery(buildGetUsernamesSQL(section));
            while (result.next()) {
                usernames.add(result.getString(2));
            }
        } catch (SQLException e) {
            throw new DatabaseException("Failed to get usernames for section: " + section, e);
        }
        return usernames;
    }
    
    public DatabaseConfiguration getConfiguration() {
        return configuration;
    }
    
    private Connection createConnection() throws DatabaseException {
        try {
            return DriverManager.getConnection(this.connectionUrl, this.configuration.getUsername(),
                    this.configuration.getPassword());
        } catch (SQLException e) {
            throw new DatabaseException("Cannot create connection to: " + this.connectionUrl, e);
        }
    }
    
    private String buildConnectionUrl() {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("jdbc:mysql://");
        urlBuilder.append(this.configuration.getHost());
        urlBuilder.append("/");
        urlBuilder.append(this.configuration.getDatabaseName());
        urlBuilder.append("?useSSL=");
        urlBuilder.append(this.configuration.isUseSSL());
        return urlBuilder.toString();
    }
    
    private String buildBatchSQLStatement() {
        StringBuilder builder = new StringBuilder();
        builder.append("INSERT IGNORE INTO `");
        builder.append(this.configuration.getDatabaseName());
        builder.append("`.`");
        builder.append(this.configuration.getTableName());
        builder.append("` (`account_names`, `sections`) VALUES (?, ?);");
        return builder.toString();
    }
    
    private String buildSelectMaxSectionSQLStatement() {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT MAX(`sections`) FROM `");
        builder.append(this.configuration.getDatabaseName());
        builder.append("`.`");
        builder.append(this.configuration.getTableName());
        builder.append("`");
        return builder.toString();
    }
    
    private String buildGetUsernamesSQL(int section) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM `");
        builder.append(this.configuration.getDatabaseName());
        builder.append("`.`");
        builder.append(this.configuration.getTableName());
        builder.append("` WHERE `sections`=");
        builder.append(section);
        builder.append(";");
        return builder.toString();
    }
}
