package de.fjobilabs.usernamedatabasetools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.fjobilabs.usernamedatabasetools.database.DatabaseConfiguration;
import de.fjobilabs.usernamedatabasetools.database.DatabaseException;
import de.fjobilabs.usernamedatabasetools.database.DatabaseHandler;
import de.fjobilabs.usernamedatabasetools.ui.CommandLineUI;
import de.fjobilabs.usernamedatabasetools.ui.UI;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 21.12.2017 - 17:08:37
 */
public abstract class CommandLineApplication {
    
    private static final int DEFAULT_CHUNK_SIZE = 1000;
    
    private String cmdLineSyntax;
    private UI ui;
    private CommandLine commandLine;
    private HelpFormatter helpFormatter;
    private Options options;
    private DatabaseHandler databaseHandler;
    
    public CommandLineApplication(String cmdLineSyntax) {
        this.cmdLineSyntax = cmdLineSyntax;
        this.ui = new CommandLineUI(false);
        this.helpFormatter = new HelpFormatter();
        this.options = new Options();
        addCommonOptions();
        addOptions(this.options);
    }
    
    public void init(String[] arguments) {
        CommandLineParser parser = new DefaultParser();
        try {
            this.commandLine = parser.parse(this.options, arguments);
        } catch (ParseException e) {
            this.ui.showInfo(e.getMessage());
            printHelp();
            System.exit(1);
            return;
        }
        
        if (this.commandLine.hasOption("h")) {
            printHelp();
            System.exit(0);
            return;
        }
        
        DatabaseConfiguration databaseConfiguration = loadDatabaseConfiguration();
        applyOptionsToConfiguration(databaseConfiguration);
        
        this.ui.setDebug(this.commandLine.hasOption("d"));
        
        if (databaseConfiguration.getChunkSize() <= 0) {
            this.ui.showInfo("database.chunkSize must be >= 1");
            System.exit(1);
            return;
        }
        
        try {
            this.databaseHandler = new DatabaseHandler(databaseConfiguration);
        } catch (DatabaseException e) {
            this.ui.showError(e.getMessage());
            this.ui.showException(e);
            System.exit(1);
            return;
        }
        
        run(this.commandLine);
    }
    
    public void printHelp() {
        this.helpFormatter.printHelp(this.cmdLineSyntax, options);
    }
    
    protected abstract void addOptions(Options options);
    
    protected abstract void run(CommandLine commandLine);
    
    public UI getUi() {
        return ui;
    }
    
    public DatabaseHandler getDatabaseHandler() {
        return databaseHandler;
    }
    
    private void addCommonOptions() {
        Option debugOption = new Option("d", "debug", false,
                "Whether debug information should be printed");
        this.options.addOption(debugOption);
        Option helpOption = new Option("h", "help", false, "Prints this help");
        this.options.addOption(helpOption);
        Option databaseHostOption = new Option("dbh", "database.host", true,
                "Database host address");
        this.options.addOption(databaseHostOption);
        Option databaseNameOption = new Option("dbn", "database.name", true, "Database name");
        this.options.addOption(databaseNameOption);
        Option databaseTableNameOption = new Option("dbtn", "database.tableName", true,
                "Database table name");
        this.options.addOption(databaseTableNameOption);
        Option databaseUsernameOption = new Option("dbun", "database.username", true,
                "Database username");
        this.options.addOption(databaseUsernameOption);
        Option databasePasswordOption = new Option("dbpw", "database.password", true,
                "Database password");
        this.options.addOption(databasePasswordOption);
        Option databaseUseSSLOption = new Option("dbssl", "database.useSSL", true,
                "Whether to use SSL for database connection");
        this.options.addOption(databaseUseSSLOption);
        Option databaseChunkSizeOption = new Option("dbcs", "database.chunkSize", true,
                "Size of a chunk of usernames (batch)");
        this.options.addOption(databaseChunkSizeOption);
    }
    
    private void applyOptionsToConfiguration(DatabaseConfiguration configuration) {
        String databaseHost = this.commandLine.getOptionValue("dbh");
        if (databaseHost != null) {
            configuration.setHost(databaseHost);
        }
        String databaseName = this.commandLine.getOptionValue("dbn");
        if (databaseName != null) {
            configuration.setDatabaseName(databaseName);
        }
        String tableName = this.commandLine.getOptionValue("dbtn");
        if (tableName != null) {
            configuration.setTableName(tableName);
        }
        String username = this.commandLine.getOptionValue("dbun");
        if (username != null) {
            configuration.setUsername(username);
        }
        String password = this.commandLine.getOptionValue("dbpw");
        if (password != null) {
            configuration.setPassword(password);
        }
        String useSSLString = this.commandLine.getOptionValue("dbssl");
        if (useSSLString != null) {
            configuration.setUseSSL(Boolean.parseBoolean(useSSLString));
        }
        String chunkSizeString = this.commandLine.getOptionValue("dbcs");
        if (chunkSizeString != null) {
            try {
                configuration.setChunkSize(Integer.parseInt(chunkSizeString));
            } catch (NumberFormatException e) {
                System.out.println("Invalid chunkSize: " + chunkSizeString);
                System.exit(1);
                return;
            }
        }
    }
    
    private DatabaseConfiguration loadDatabaseConfiguration() {
        DatabaseConfiguration configuration = new DatabaseConfiguration();
        Properties properties = new Properties();
        InputStream in = CommandLineApplication.class.getResourceAsStream("/database.properties");
        if (in != null) {
            try {
                properties.load(in);
            } catch (IOException e) {
                System.err.println("Failed to load internal default properties");
                e.printStackTrace();
            }
        }
        Properties externalProperties = new Properties();
        try {
            in = new FileInputStream("database.properties");
            try {
                externalProperties.load(in);
            } catch (IOException e) {
                System.err.println("Failed to load external properties");
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            // We can silently ignore this
        }
        properties.putAll(externalProperties);
        
        configuration.setHost(properties.getProperty("database.host"));
        configuration.setDatabaseName(properties.getProperty("database.name"));
        configuration.setTableName(properties.getProperty("database.tableName"));
        configuration.setUsername(properties.getProperty("database.username"));
        configuration.setPassword(properties.getProperty("database.password"));
        configuration.setUseSSL(Boolean.parseBoolean(properties.getProperty("database.useSSL")));
        String chunkSizeString = properties.getProperty("database.chunkSize");
        try {
            configuration.setChunkSize(Integer.parseInt(chunkSizeString));
        } catch (NumberFormatException e) {
            configuration.setChunkSize(DEFAULT_CHUNK_SIZE);
        }
        return configuration;
    }
}
