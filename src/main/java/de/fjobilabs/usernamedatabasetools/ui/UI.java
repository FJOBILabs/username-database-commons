package de.fjobilabs.usernamedatabasetools.ui;


/**
 * @author Felix Jordan
 * @version 1.0
 * @since 17.12.2017 - 16:00:30
 */
public interface UI {
    
    public void showInfo(String message);
    
    public void showWarning(String message);
    
    public void showError(String message);
    
    public void showException(Exception e);
    
    public boolean isDebug();
    
    public void setDebug(boolean debug);
}
