package de.fjobilabs.usernamedatabasetools.ui;

/**
 * @author Felix Jordan
 * @version 1.0
 * @since 17.12.2017 - 16:00:14
 */
public class CommandLineUI implements UI {
    
    private boolean debug;
    
    public CommandLineUI(boolean debug) {
        this.debug = debug;
    }
    
    @Override
    public void showInfo(String message) {
        System.out.println(message);
    }
    
    @Override
    public void showWarning(String message) {
        System.out.println("Warning: " + message);
    }
    
    @Override
    public void showError(String message) {
        System.out.println("Error: " + message);
    }
    
    @Override
    public void showException(Exception e) {
        if (this.debug) {
            e.printStackTrace(System.out);
        }
    }
    
    @Override
    public boolean isDebug() {
        return debug;
    }
    
    @Override
    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
